<?php 
    /* Repetition Control Structures */
    //While Loops - takes a single condition, if the condition is met the code inside the block will continue to run

    function whileLoop(){
    $count = 5;
    while($count !== 0){ //condition     
        //display the value of the $count
        echo $count."<br/>";
        //iterator
        $count--;
    }
}
    /* 
        5 <br/>
        4 <br/>
        3 <br/>
        2 <br/>
        1 <br/>
    */

/* 
    Mini Activity:

    1. Create a function called 'printDivisibleBy5' that shall print the numbers that are divisible by 5 in between 1-100.

    2.Print the output on index.php

    5mins

*/

    function printDivisibleBy5(){
    $numbers=1;
        for($numbers = 0;$numbers <= 100; $numbers++){
            if($numbers % 5 === 0){
                echo $numbers."<br/>";
            }
        }
    }
    // function printDivisibleBy5(){
    //     $num = 1;
    //     while($num <= 100){
    //         if ($num % 5 === 0) {
    //             echo $num."<br/>";
    //         }
    
    //         $num++;
    //     }
    // }

    //Do-While Loops
        function doWhileLoop(){
            $count = 0;
            do {
                echo $count."<br/>";
                $count++;
            } while($count <=20);
        }

        /*
            Mini-Activity:

            1.create a function called 'getOddNumbers' that print the odd numbers in bet ween 29-60 using do-while loop
            2.Display the result of getOddNumbers to the index.php
        */

        function getOddNumbers(){
            $count = 29;
            do {
                if($count % 2 != 0){
                    echo $count."<br/>";
                }
                $count++;
            } while($count <=60);
        }
    
    //For loops
    //print the even numbers between 0-25
    function forLoop(){
        for($count = 0;$count <= 20; $count++){
            if($count % 2 == 0){
                echo $count."<br/>";
            }
        }
    }

    $firstName = "John Smith";

    //Array Manipulation

    //array() or [] square brackets
    $tasks = ['drink html', 'eat javascript', 'inhale css'];
    $computerBrands = array('Lenovo', 'HP', 'Acer', 'Asus');

    //Associative Array - has a set of key values pairs as its element


    $gradePeriods = [
        'firstGrade' => 98.5,
        'secondGrade' => 90.8,
        'thirdGrade' => 89.2,
        'fourthGrade' => 90.1
    ];

    //Array Manipulation

    //Insert elements inside an array

    array_push($tasks, "bake sass", "cook bootstrap");

    //alternative way of pushing elements inside an array
    $tasks['chop nodejs'];

    // array.push($gradePeriods, "finalGrade" => 90.5);
    // if we are going to push an key-value pair to an associative array, it can
    // be done by the ff:

    $gradePeriods["finalGrade"] = 90.5;

    /* 
        Mini-Activity:
        1.Create an empty array called students.
        2.Then push the following elements:
    
            "juan" -> subjects -> english
                               -> filipino
                               -> math
            peter -> subjects -> english
                              -> math
                              -> biology
            
    */

    $student = [];

    $student[0] = [
        "studentName" => "juan",
        "subjects" => ["english", "filipino", "math"] 
    ];

    $student[1] = [
        "studentName" => "juan",
        "subjects" => ["english", "filipino", "math"] 
    ]; 

    
    function printTasks($taskList){
        //for loop
        $arrLength = count($taskList); //returns the count of elements inside $tasks array
        // var_dump($arrLength);
        for($count = 0; $count < $arrLength; $count++){
            echo "<li>$taskList[$count]</li>";
        }
    }

    /*
        Mini Activity

        1. create function called 'printComputers' that shall iterate the $computerBrands array in the index.php *tip: make sure to iterate your values inside a ul tag
    */

    function printComputers($compList){
        $arrLength = count($compList); 
        for($count = 0; $count < $arrLength; $count++){
            echo "<li>$compList[$count]</li>";
        }
    }

    //Foreach loops

    function foreachLoop($taskList){
        //for each loop iteration, the values from the array $taskList is assigned to $element, until it reaches, the last array element 
        foreach($taskList as $element){
            echo "$element <br/>";
        }
    }

    function printGrades($gradeList){
        //$key shall contain the keynames of the associative array
        //'firstgrading' => 98.2
        foreach($gradeList as $key => $grade){
            echo "<li> $key: $grade </li>";
        }
    }

    $heroes = [
        'marvel' => [
            "teamCaptain" => ['captain america', 'bucky barnes', 'falcon', 'scarlet witch'],

            "teamIron" => ['iron man', 'spider man', 'black widow', 'black panther'],

        ],
            
        'dc' => [
            "teamSuperman" => ['aquaman', 'wonder woman', 'supergirl'],
            "teamBatman" => ['robin', 'batgirl', 'alfred']
        ],
        'xmen' => [
            "teamProfX" => ['wolverine', 'storm', 'jean grey'],
            "teamMagneto" => ['mystique', 'juggernaut', 'pyro']
        ]
        
    ];

    // loop nested associative array -> nested foreach loop
		function printHeroes($heroesList){
			foreach($heroesList as $heroes => $teams){
				//print_r($heroes);
				//print_r($teams);
                echo "<h2> $heroes </h2>";
                foreach($teams as $teamnames =>$players){
                    // print_r($teamnames);
                    // print_r($players);
                    echo "<h4> $teamnames </h4>";
                    foreach($players as $player){
                        echo "<li>$player</li>";
                    }
                }
			}
		}

    //array_unshift(), array_pop() -> removes elements on an array
    array_pop($tasks); // removes the last element of an array
    

    //Reassigning values to a variable
    $firstName = "John";
    $firstName = "Peter";
?>