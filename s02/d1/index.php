<?php require "./phpOperation.php" ?>
<!-- imports the phpOperation file inside our index file. -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>PHP Selection/Loop Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Repetition Control Structures</h1>

    <h2>While Loop</h2>

    <span>
        <?php whileLoop(); ?>
    </span>

    <h2>For Loop</h2>

    <span>
        <?php printDivisibleBy5(); ?>
    </span>

    <h2>Do-While Loop</h2>

    <span>
        <?php doWhileLoop(); ?>
    </span>

    <h2>Do-While Loop ACt</h2>
    <span>
        <?php getOddNumbers(); ?>
    </span>

    <h2>For Loop</h2>

    <span>
        <?php forLoop(); ?>
    </span>

    <h3>Associative Array</h3>

    <p>
        <?php echo $gradePeriods['fourthGrade']; ?>
    </p>

    <p>
        <?php var_dump($tasks); ?>
    </p>

    <p>
        <?php var_dump($gradePeriods); ?>
    </p>

    <p>
        <?php var_dump($student); ?>
    </p>

    <p>
        <?php var_dump($student[0]); ?>
    </p>

    <p>
        <?php
            // var_dump($student[0]['studentName']); 
            print_r($student[0]); 
        ?>
    </p>

     <!-- mas pretty way of using print_r -->
    <!-- -->
     <pre>
        <?php print_r($student[0]['studentName']); ?>
    </pre>

    <ul>
        <?php printTasks($tasks); ?>
    </ul>

    <ul>
        <?php printComputers($computerBrands); ?>
    </ul>

    <?php 
        foreachLoop($tasks);
    ?>

<?php 
        printGrades($gradePeriods);
    ?>

    <ul>
        <?php 
            printHeroes($heroes);
        ?>
    </ul> 
    
       

    <h2>Array Manipulation</h2>
</body>
</html>